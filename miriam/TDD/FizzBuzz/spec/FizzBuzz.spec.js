import { FizzBuzz } from '../src/FizzBuzz.js'

describe('FizzBuzz', () => {

    //como poner aqui para listar los numeros del 1 al 100?

    it('should return "Fizz" when the number is divisible by 3', () => {
        // Arrange
        //Instanciar la clase
        const fizzBuzz = new FizzBuzz()
        const num = 12
        const fizz = "Fizz"

        // Act
        const result = fizzBuzz.comprobador(num)
       

        // Assert
       expect(result).toBe(fizz)
    })

    it('should return "Buzz" when the number is divisible by 5', () => {
        //Arrange
        const fizzBuzz = new FizzBuzz()
        const num = 20
        const buzz = "Buzz"

        //Act
        const result = fizzBuzz.comprobador(num)

        //Assert
        expect(result).toBe(buzz)


    })

    it('should return "FizzBuzz" when the number is divisible by 3 and by 5', () => {
        //Arrange
        const fizzBuzzClass = new FizzBuzz()
        const num = 15
        const fizzBuzz = "FizzBuzz"

        //Act
        const result = fizzBuzzClass.comprobador(num)

        //Assert
        expect(result).toBe(fizzBuzz)

    })

    it('should return "number" when the number is not divisible neither by 3 or 5', () => {
        //Arrange
        const fizzBuzz = new FizzBuzz()
        const num = 17
        const numberStr= "17"

        //Act
        const result = fizzBuzz.comprobador(num)

        //Assert
        expect(result).toBe(numberStr)


    })

    it('should return "Fizz" if it has a 3 in it', () => {
        //Arrange
        const fizzBuzz = new FizzBuzz()
        const num = 13
        const fizz = "Fizz"

        //Act
        const result = fizzBuzz.comprobador(num)

        //Assert
        expect(result).toBe(fizz)
    })

    it('should return "Buzz" if it has a 5 in it', () => {
        //Arrange
        const fizzBuzz = new FizzBuzz()
        const num  = 52
        const buzz = "Buzz"

        //Act 
        const result = fizzBuzz.comprobador(num)

        //Assert
        expect(result).toBe(buzz)

    })

})
