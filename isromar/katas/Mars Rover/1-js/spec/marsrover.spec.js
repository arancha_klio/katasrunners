import { marsRover } from '../src/marsrover.js'

describe('marsRover', () => {
    it('should return same final position than initial', () => {
        // Arrange
        const expected_value = '5,5,N'
        const landingPosition = '5,5,N'
        const command = ''

        // Act
        const result = marsRover(command,landingPosition)

        // Assert
        expect(result).toBe(expected_value)
    })
})
