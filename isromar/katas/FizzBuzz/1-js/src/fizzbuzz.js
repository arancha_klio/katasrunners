export function fizzbuzz(numero_que_recibe_del_test) {
    // Yellow belt
    const fizz = 3;
    const buzz = 5;
    var valor_a_dividir;

    // Comprueba si un número es divisible por el número dado como divisor
    function es_divisible_por(dividendo, divisor){
        return (dividendo % divisor == 0);
    }

    // Comprueba si una cifra forma parte de un número
    function hay_cifra_en_numero(numero_donde_buscar, cifra_a_buscar){
        numero_donde_buscar = numero_donde_buscar.toString();
        cifra_a_buscar = cifra_a_buscar.toString();
        if (numero_donde_buscar.match(cifra_a_buscar) == null){
            return false;
        } return true;
    }

    // Devuelve un texto u otro según el valor del número recibido
    function que_palabra_poner(valor_a_dividir){
        if ((es_divisible_por(valor_a_dividir, fizz) || hay_cifra_en_numero(valor_a_dividir, fizz)) && (es_divisible_por(valor_a_dividir, buzz) || hay_cifra_en_numero(valor_a_dividir, buzz))) {
            return 'FizzBuzz';
        }
        if (es_divisible_por(valor_a_dividir, fizz) || hay_cifra_en_numero(valor_a_dividir, fizz)) {
            return 'Fizz';
        }
        if (es_divisible_por(valor_a_dividir, buzz) || hay_cifra_en_numero(valor_a_dividir, buzz)) {
            return 'Buzz';
        } else {
            return valor_a_dividir.toString();
        }
    }

    return que_palabra_poner(numero_que_recibe_del_test);
}