import { FizzBuzz } from '../src/fizzbuzz.js'

describe('FizzBuzz', () => {
    it('should return "Fizz" if divisible by 3', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = 'Fizz'

        // Act
        var valorDevuelto = fizzBuzz.quePalabraPoner(3)

        // Assert
        expect(valorDevuelto).toBe(expected_value)
    })

    it('should return "Buzz" if divisible by 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = 'Buzz'

        // Act
        var valorDevuelto = fizzBuzz.quePalabraPoner(5)

        // Assert
        expect(valorDevuelto).toBe(expected_value)
    })

    it('should return "FizzBuzz" if divided by 3 and 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = 'FizzBuzz'

        // Act
        var valorDevuelto = fizzBuzz.quePalabraPoner(15)

        // Assert
        expect(valorDevuelto).toBe(expected_value)
    })

    it('should return String if not divided by nor 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = '17'

        // Act
        var valorDevuelto = fizzBuzz.quePalabraPoner(17)

        // Assert
        expect(valorDevuelto).toBe(expected_value)
    })

})