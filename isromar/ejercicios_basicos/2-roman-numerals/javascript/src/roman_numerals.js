export function convertToRoman(num) {
    //La clave está en indicar la equivalencia también de los números 4,9,40,90 etc. no solo las conocidas

    //Separa el número en millares, centenas, decenas y unidades para convertir cada cifra por separado
    var millares = Math.trunc(num/1000);
    var centenas = Math.trunc((num-(millares*1000))/100);
    var decenas = Math.trunc((num-(millares*1000 + centenas*100))/10);
    var unidades = num-(millares*1000 + centenas*100 + decenas*10);

    //Array para almacenar los números romanos
    var numeros_romanos=[];

    /*** Inicia por millares porque al hacer push() irá añadiendo items al final del array
    Las equivalencias a considerar son 1,4,5,9 Al separar el número cada cifra se puede evaluar del 1 al 9,
    no del 10 al 90, ni del 100 al 900, etc. y en cada caso se equipara con su correspondiente en romanos
    M   C,CD,D,CM     X,XL,L,XC    I,IV,V,IX  ***/

    //Asigna la equivalencia romana de millares según la cifra árabe dada y pone mensaje de error si esta es mayor que 4000
    if (millares <=3 ){
        for (var i=1;i<=millares;i++){
            numeros_romanos.push("M");
        }
    } else if (millares == 4){
        console.log("Número no válido, ha de ser menor de 4000");
    }

    //Asigna la equivalencia romana de centenas según la cifra árabe
    if (centenas <=3 ){
        for (var i=1;i<=centenas;i++){       //Añade tantas C según la cifra sea 1,2 ó 3
            numeros_romanos.push("C");
        }
    } else if (centenas == 4){
        numeros_romanos.push("CD");
    } else if (centenas >=5 && centenas <= 8){
        numeros_romanos.push("D");                   //Añade D si la cifra es 5,6,7 u 8
        for (var i=6;i<=centenas;i++){       //Añade tantas C según la cifra sea 6,7 u 8
            numeros_romanos.push("C");
        }
    } else if (centenas == 9){
        numeros_romanos.push("CM");
    }

    //Asigna la equivalencia romana de decenas según la cifra árabe
    if (decenas <=3 ){
        for (var i=1;i<=decenas;i++){       //Añade tantas X según la cifra sea 1,2 ó 3
            numeros_romanos.push("X");
        }
    } else if (decenas == 4){
        numeros_romanos.push("XL");
    } else if (decenas >=5 && decenas <= 8){
        numeros_romanos.push("L");                  //Añade L si la cifra es 5,6,7 u 8
        for (var i=6;i<=decenas;i++){       //Añade tantas X según la cifra sea 6,7 u 8
            numeros_romanos.push("X");
        }
    } else if (decenas == 9){
        numeros_romanos.push("XC");
    }

    //Asigna la equivalencia romana de unidades según la cifra árabe
    if (unidades <=3 ){
        for (var i=1;i<=unidades;i++){       //Añade tantas I según la cifra sea 1,2 ó 3
            numeros_romanos.push("I");
        }
    } else if (unidades == 4){
        numeros_romanos.push("IV");
    } else if (unidades >=5 && unidades <= 8){
        numeros_romanos.push("V");                   //Añade V si la cifra es 5,6,7 u 8
        for (var i=6;i<=unidades;i++){       //Añade tantas I según la cifra sea 6,7 u 8
            numeros_romanos.push("I");
        }
    } else if (unidades == 9){
        numeros_romanos.push("IX");
    }

    //Une todo el array para que no tenga , entre los items
    var numeros_romanos = numeros_romanos.join('');

    //Escribe el número dado en números romanos
    console.log("El número " + num + " en números romanos es: " + numeros_romanos + "\n");

    //Devuelve el valor en números romanos
    return numeros_romanos;
}
