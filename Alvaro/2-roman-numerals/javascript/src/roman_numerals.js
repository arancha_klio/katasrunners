export function convertToRoman(num) {
    let result_symbol = '' 
     const roman_and_arabic = 
    {
        'M':1000,
        'CM':900,
        'D':500,
        'CD':400,
        'C':100,
        'XC':90,
        'L':50,
        'XL':40,
        'X':10,
        'IX':9,
        'V':5,
        'IV':4,
        'I':1
    }

    for(let key in roman_and_arabic){
        while(num >= roman_and_arabic[key]){
            result_symbol += key
            num -= roman_and_arabic[key]
        }
    }
    console.log(result_symbol)  
    return result_symbol 
}


    // Bucle for in/off para iterar con objetos, nos piden saber el valor de 350 en números romanos.
    // La primera línea declaramos la key o el value de nustro objeto.
    // En el while declaramos la condición que necesitamos, aquí recibimos un número(350) que decimos que mientras sea mayor o igual a la [key]del objeto,
    //vaya iterando por el objeto, por ejemplo entra en M:1000 va a CM:900 así hasta que para en el que sea menor o igual C:100.
    // A result_symbol le sume la key, el resultado será la suma de keys, en el ejemplo sería C
    //A number le restamos la [key], sería 350 - 100.
    //En la siguiente iteración, al while entra num(ahora vale 250) mientras sea mayor o igual a el objeto vaya iterando.
    //Después del bucle anterios result_symbol vale C y hacemos + = key, ahora vale CC.
    //En la iteración siguiente num(250) le -= la [key] del objeto que son 100 = 150.
    //Siguiendo esta lógica la siguiente iteración num valdrá 50 y result_symbol CCC.
    //En esta iteración num valdrá 0 y result_symbol CCCL, esto dará lugar a que tengamos como resultado que el número 350,
    //después de varias iteraciones por el objeto nos de como resultado 305 = CCCl 














    // for(let i = 0; i < arabic_numbers.length; i++){
    //     while(num >= arabic_numbers[i]) {
    //         result_numbers += roman_numbers[i] 
    //         num -= arabic_numbers[i]
    //     }
    // }
    // return result_numbers

