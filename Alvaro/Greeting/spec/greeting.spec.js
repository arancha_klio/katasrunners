import { Greeting } from '../src/greeting.js'

describe('Greeting', () => {
    it('should return hello + name, when given a name', () => {
        // Arrange
        const greeting = new Greeting
        const expected_value = 'hello, anchoa'

        // Act
        const givenTheName = greeting.greet('anchoa')

        // Assert
        expect(givenTheName).toBe(expected_value)
    })

    it('Handle nulls by introducing a stand-in', () => {
        const greeting = new Greeting
        const expected_value = "Hello, my friend"

        const givenTheName = greeting.greet()


        expect(givenTheName).toBe(expected_value)
    })

    it('should return the greet in uppercase when the name is give in uppercase', () => {
        const greeting = new Greeting
        const expected_value = "HELLO JERRY!"

        const givenTheName = greeting.greet("JERRY")

        expect(givenTheName).toBe(expected_value)
    })

    it(' when name is an array with two names, both names should be printed', () => {
        const greeting = new Greeting
        const expected_value = "Hello, Jill and Jane."

        const givenTheName = greeting.greet(["Jill", "Jane"])

        expect(givenTheName).toBe(expected_value)
    })

    it('When name represents more than two names, separate them with commas and close with a comma and "and".', () => {
        const greeting = new Greeting
        const expected_value = "Hello, Amy, Brian and Charlotte."

        const givenTheName = greeting.greet(["Amy", "Brian", "Charlotte"])

        expect(givenTheName).toBe(expected_value)
    })

    it('Allow mixing of normal and shouted names by separating the response into two greetings', () => {
        const greeting = new Greeting
        const expected_value = "Hello, Amy and Charlotte. AND HELLO BRIAN!"

        const givenTheName = greeting.greet(["Amy", "BRIAN", "Charlotte"])

        expect(givenTheName).toBe(expected_value)
    })

    it('If any entries in name are a string containing a comma, split it as its own input', () => {
        const greeting = new Greeting
        const expected_value = "Hello, Bob, Charlie and Dianne."

        const givenTheName = greeting.greet(["Bob", "Charlie, Dianne"])

        expect(givenTheName).toBe(expected_value)
    })
})
