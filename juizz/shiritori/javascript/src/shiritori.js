const TIMEOVER = 10

export class Shiritori {
    
    constructor(){
        this.words = []
    }

    turn(new_word, timeTurn=0) {
        if(this.firstTurn()){
            this.words.push(new_word)
            return 'Ok'
        }

        if (this.rules(new_word, timeTurn)){
            this.words.push(new_word)
            return 'Ok'
        }

        return 'Game over'
    }

    rules(new_word, timeTurn){
        return this.wordNotUsed(new_word) && 
        this.isOnTime(timeTurn) && 
        this.link(new_word)
    }

    wordNotUsed(new_word){
        return !this.words.includes(new_word)
    }

    isOnTime(timeTurn){
        return timeTurn <= TIMEOVER
    }
    
    link(next_word){
        return this.takingEnd(this.lastWord()) == this.takingFront(next_word)
    }

    takingEnd(word){
        return word.slice(-1)
    }

    takingFront(word){
        return word.slice(0,1)
    }

    lastWord(){
        return this.words.slice(-1)[0]
    }
    
    firstTurn(){
        return this.words.length == 0
    }
 }
