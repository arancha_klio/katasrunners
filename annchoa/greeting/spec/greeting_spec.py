from mamba import *
from expects import *
from greeting import *

with description('Greeting') as self:
    with it('string with a name'):
        greet = Greet()
        result = greet.sentence(["Bob"])
        expect(result).to(equal("Hello, Bob."))

    with it('If you dont know the name, so my friend'):
        greet = Greet()
        result = greet.sentence([])
        expect(result).to(equal("Hello, my friend."))

    with it('name in UPPER CASE'):
        greet = Greet()
        result = greet.sentence(["JERRY"])
        expect(result).to(equal("HELLO, JERRY!"))

    with it('multiple in UPPER CASE'):
        greet = Greet()
        result = greet.sentence(["JERRY", "ANDREA", "DAVID"])
        expect(result).to(equal("HELLO, JERRY, ANDREA AND DAVID!"))

    with it('two names'):
        greet = Greet()
        result = greet.sentence(["Jill", "Jane"])
        expect(result).to(equal("Hello, Jill and Jane."))

    with it('last name with and'):
        greet = Greet()
        result = greet.sentence(["Jill", "Jane", "Amapola", "Inti"])
        expect(result).to(equal("Hello, Jill, Jane, Amapola and Inti."))

    with it('mixed'):
        greet = Greet()
        result = greet.sentence(["Jill", "Jane", "AMAPOLA", "Inti"])
        expect(result).to(equal("Hello, Jill, Jane and Inti. AND HELLO, AMAPOLA!"))

    with it('multiple names in single string'):
        greet = Greet()
        result = greet.sentence(["Jill, Jane", "AMAPOLA", "Inti"])
        expect(result).to(equal("Hello, Jill, Jane and Inti. AND HELLO, AMAPOLA!"))
