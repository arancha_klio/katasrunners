const roman_numerals = {
                    'M':1000,
                    'CM':900,
                    'D':500,
                    'CD':400,
                    'C':100,
                    'XC':90,
                    'L':50,
                    'XL':40,
                    'X':10,
                    'IX':9,
                    'V':5,
                    'IV':4,
                    'I':1
                    }

export function convertToRoman(num) {
    var result = ''

      for (var simbol in roman_numerals){
        while (num >= roman_numerals[simbol]){
          result += simbol
          num -= roman_numerals[simbol]
        }
      }
      return result
}

/* por qué no hace lo mismo cuando {
  4: 'IV'
  1: 'M'
 }

entonces pone las claves en orden ascendente
*/
