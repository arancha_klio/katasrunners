
class Converter_Roman_Numbers
 
  def convert_numbers(number)
    @number = number

    convert_arabic_numbers_to_roman_numbers
  end

private

  ROMAN_SYMBOLS = { 
    1000 => "M", 900 => "CM", 500 => "D", 400 => "CD",
    100 => "C", 90 => "XC", 50 => "L", 40 => "XL",
    10 => "X", 9 => "IX", 5 => "V", 4 => "IV",
    1 => "I" 
  }

  def convert_arabic_numbers_to_roman_numbers
    roman_numeral = ""
    arabic_number = @number

    ROMAN_SYMBOLS.each do |arabic, roman| 
      while arabic_number >= arabic
        roman_numeral << roman
        arabic_number -= arabic
      end
    end
    roman_numeral
  end

end