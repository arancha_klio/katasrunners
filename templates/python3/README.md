# Python kata template

Esta plantilla utiliza la librería de por defecto de [unittest](https://docs.python.org/3.5/library/unittest.html) de Python para ejecutar los tests. Por tanto, no necesita dependencias externas fuera de Python 3.

## Ejecutando los tests

`python3 -m unittest test_example.py`

## Instalación

- Instala Python 3. Puedes encontrar los detalles [en su web](https://www.python.org/downloads/).
