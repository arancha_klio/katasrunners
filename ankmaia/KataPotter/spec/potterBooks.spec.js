import { PotterBooks } from '../src/potterBooks.js'

describe('PotterBooks', () => {
    it("Calculate price for a book", () => {
        // Arrange
        const basePrice = 8
        const potterBooks = new PotterBooks()
        // Act
        const totalPrice = potterBooks.calculatePrice({'Harry Potter y la piedra filosofal':1})

        // Assert
        expect(totalPrice).toEqual(basePrice)
    })
    
    it("Calculate price for two equal books", () => {
        // Arrange
        const priceForTwoBooks = 16
        const potterBooks = new PotterBooks()
        // Act
        const totalPrice = potterBooks.calculatePrice({'Harry Potter y la piedra filosofal':2})
         // Assert
         expect(totalPrice).toEqual(priceForTwoBooks)
    })

    it("Calculate price for two different books", () => {
        // Arrange
        const priceForTwoDifferentBooks = 15.2
        const potterBooks = new PotterBooks()
        // Act
        const totalPrice = potterBooks.calculatePrice({'Harry Potter y la piedra filosofal':1,'Harry Potter y la Orden del Fénix':1})
         // Assert
         expect(totalPrice).toEqual(priceForTwoDifferentBooks)
    })

    it("Calculate price for three different books", () => {
        // Arrange
        const priceForThreeDifferentBooks = 21.60
        const potterBooks = new PotterBooks()
        // Act
        const totalPrice = potterBooks.calculatePrice({'Harry Potter y la piedra filosofal':1,'Harry Potter y la Orden del Fénix':1,'Harry Potter y el príncipe mestizo':1})
         // Assert
         expect(totalPrice).toEqual(priceForThreeDifferentBooks)
    })
})
