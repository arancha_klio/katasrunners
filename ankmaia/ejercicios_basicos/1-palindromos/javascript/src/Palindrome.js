const ZERO = 0
const UNO = 1
export class Palindrome{
    constructor(){
        this.stringFormatted = ""
        this.stringReversed = "" 
    }

    isPalindrome(String){
        this.convertStringToRightFormat(String)
        this.reverseTheString(this.stringFormatted)
        if(this.checkIfPalindrome(this.stringFormatted)){
            return true
        }
        return false
        
    }

    checkIfPalindrome(String){
        if(String === this.stringReversed) {
            return true
        }
        return false
    }

    reverseTheString(String){
        const characters = String.length-UNO
        for(let i =characters; i>=ZERO ;i--){
            this.stringReversed += String[i] 
        }
        return this.stringReversed
    }

    stringToLower(String){
        this.stringFormatted = String.toLowerCase()
    }

    replaceACharacter(character){
        this.stringFormatted = this.stringFormatted.replace(character,"")
    }
    
    convertStringToRightFormat(String){
        this.stringToLower(String)
        this.replaceACharacter(',')
        this.replaceACharacter('_')
        this.replaceACharacter('.')
    }
    
    /*Mejorar el método convertStringToRightFormat(String) con
    una regexp y el método String.replace(regexp,"")*/
  
   
}
