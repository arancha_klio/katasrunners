# Cifrado César

Uno de los cifrados más sencillos y conocidos es el Cifrado César, tambien conocido como cifrado de cambio. En un cifrado de cambio las letras son cambiadas por otras letras basado en un número.

Un uso común es el cifrado [ROT13](https://en.wikipedia.org/wiki/ROT13), donde los valores de las letras son reemplazados por la letra que está 13 posiciones más adelante en el abecedario. Por tanto, `A` <-> `N`, `B` <-> `O`...

Escribe una función que tome una cadena de texto codificada con ROT13 y la devuelva decodificada.

Todas las letras estarán en mayúscula. No transformes ningún caracter no alfanúmerico (espacios, puntuación...), pero mantenlos en el string devuelto.

## Instalación y uso

### Javascript

#### Ejecutando los tests

`npm test`

#### Instalación

- Primero instala NodeJS. Puedes encontrar los detalles [en su web](https://nodejs.org/en/). Esto instalará `Node` y `npm`.
- Instala las dependencias utilizando `npm`: `npm install`

### Python

#### Ejecutando los tests

`python3 -m unittest test_example.py`

#### Instalación

- Instala Python 3. Puedes encontrar los detalles [en su web](https://www.python.org/downloads/).
