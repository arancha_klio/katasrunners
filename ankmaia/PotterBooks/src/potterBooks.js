export class PotterBooks {
    constructor(books){
        this.amount = 8
        this.books = books
        this.uniqueBooks = [...new Set(this.books)]
    }
    
    price(){
       
        if (this.isUniqueBook()){
            return this.amount *this.books.length
        }
    

        if (this.books.length == this.uniqueBooks.length){
            switch(this.uniqueBooks.length){
                case 2:
                    return this.isFiveDiscount()
                    break
                
                case 3:
                    return this.isTenDiscount()
                    break
            }
        }


        const amountBookWithoutDiscount = (this.books.length - this.uniqueBooks.length) * this.amount
        switch(this.uniqueBooks.length){
            case 2:
                return this.isFiveDiscount() + amountBookWithoutDiscount
                break
            
            case 3:
                return  this.isTenDiscount() + amountBookWithoutDiscount
                break
        }
       
    }
    isFiveDiscount(){
        return this.amount * 0.95 * this.uniqueBooks.length
    }
    isTenDiscount(){
        return this.amount * 0.9 * this.uniqueBooks.length
    }
    isUniqueBook(){
        return this.uniqueBooks.length == 1
    }
    carmela(){
        let acum = 0
        for(const book of this.books){
            let count = 0;
            for(const otherBooks of this.books){
                if(book==otherBooks){
                    count+=1
                }
            }
            console.log("count:" + count)
            if(acum<count){
                acum = count 
                console.log( "acum" + acum)
            }

        }
        return acum
    }
    
}
