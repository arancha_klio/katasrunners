export function FizzBuzz(numero) {
    // Good luck!
    if(numero == null) numero = 100
    var salida = ""
    var x = 1
    while(x<=numero) {
        if(x%3 == 0 && x%5 == 0) {
            salida += "FizzBuzz"
            if(x<numero) salida += " "
            x++
            continue
        }

        if(x%3 == 0) {
            salida += "Fizz"
            if(x<numero) salida += " "
            x++
            continue
        }

        if(x%5 == 0) {
            salida += "Buzz"
            if(x<numero) salida += " "
            x++
            continue
        }

        salida += x;
        if(x<numero) salida += " "
        x++
        

    }
    return salida
}


