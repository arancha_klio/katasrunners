export function rot13(str) { // LBH QVQ VG!
  function isALetter(charVal) {
      if( charVal.toUpperCase() != charVal.toLowerCase() )
        return true;
      else
        return false;
    }

  str = str.toUpperCase();
  var temp = "";
  for(var x=0; x<=str.length; x++) {
    if(isALetter(str.charAt(x)) == false) {
      temp += str.charAt(x);
    } 
    else {
      var ascii = str.charCodeAt(x)+13;
      if(ascii > 90) ascii = ascii-26;
      temp += String.fromCharCode(ascii); 
    }
  }
  return temp;
}

export function deRot13(str) {
  function isALetter(charVal) {
    if( charVal.toUpperCase() != charVal.toLowerCase() )
      return true;
    else
      return false;
  }

str = str.toUpperCase();
var temp = "";
for(var x=0; x<=str.length; x++) {
  if(isALetter(str.charAt(x)) == false) {
    temp += str.charAt(x);
  } 
  else {
    var ascii = str.charCodeAt(x)-13;
    if(ascii < 65) ascii = ascii+26;
    temp += String.fromCharCode(ascii); 
  }
}
return temp;
}