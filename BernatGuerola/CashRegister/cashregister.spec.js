import { checkCashRegister } from '../src/cashregister.js'

describe('Example1', () => {


    it('should return object', () => {
        // Arrange
        const expected_value = true
        const price = 19.5
        const cash = 20
        const cid = [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]

        // Act
        const result = checkCashRegister(price, cash, cid) 

        // Assert
        expect(typeof result).toEqual('object')

    })

    it('should return open', () => {
        // Arrange
        const expected_value = {status: "OPEN", change: [["QUARTER", 0.5]]}
        const price =19.5
        const cash=20
        const cid=[["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]

        // Act
        const result = checkCashRegister(price, cash, cid) 

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('should return open2', () => {
        // Arrange
        const expected_value = {status: "OPEN", change: [["TWENTY", 60], ["TEN", 20], ["FIVE", 15], ["ONE", 1], ["QUARTER", 0.5], ["DIME", 0.2], ["PENNY", 0.04]]}
        const price=3.26
        const cash=100
        const cid=[["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]

        // Act
        const result = checkCashRegister(price, cash, cid)

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('should return INSUFFICIENT_FUNDS', () => {
        // Arrange
        const expected_value = {status: "INSUFFICIENT_FUNDS", change: []}
        const price=19.5
        const cash=20
        const cid=[["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]

        // Act
        const result = checkCashRegister(price, cash, cid) 

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('should return INSUFFICIENT_FUNDS2', () => {
        // Arrange
        const expected_value = {status: "INSUFFICIENT_FUNDS", change: []}
        const price=19.5
        const cash=20
        const cid=[["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 1], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]

        // Act
        const result = checkCashRegister(price, cash, cid)

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('should return CLOSED', () => {
        // Arrange
        const expected_value = {status: "CLOSED", change: [["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]}
        const price=19.5
        const cash=20
        const cid=[["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]

        // Act
        const result = checkCashRegister(price, cash, cid) 

        // Assert
        expect(result).toEqual(expected_value)
    })
})