public class Datos {
	
	int onehundred;
	int twenty;
	int ten;
	int five;
	int one;
	int quarter;
	int dime;
	int nickel;
	int penny;
	Double totalmoney = onehundred*100+twenty*20+ten*10+five*5+quarter*0.25+dime*0.1+nickel*0.05+penny*0.01;
	
	public Datos(int onehundred, int twenty, int ten, int five, int one, int quarter, int dime, int nickel, int penny) {
		super();
		this.onehundred = onehundred;
		this.twenty = twenty;
		this.ten = ten;
		this.five = five;
		this.one = one;
		this.quarter = quarter;
		this.dime = dime;
		this.nickel = nickel;
		this.penny = penny;
	}

	public int getOnehundred() {
		return onehundred;
	}

	public void setOnehundred(int onehundred) {
		this.onehundred = onehundred;
	}

	public int getTwenty() {
		return twenty;
	}

	public void setTwenty(int twenty) {
		this.twenty = twenty;
	}

	public int getTen() {
		return ten;
	}

	public void setTen(int ten) {
		this.ten = ten;
	}

	public int getFive() {
		return five;
	}

	public void setFive(int five) {
		this.five = five;
	}

	public int getOne() {
		return one;
	}

	public Double getTotalmoney() {
		return (this.onehundred*100+this.twenty*20+this.ten*10+this.five*5+this.quarter*0.25+this.dime*0.1+this.nickel*0.05+this.penny*0.01);
	}

	public void setTotalmoney(Double totalmoney) {
		this.totalmoney= this.onehundred*100+this.twenty*20+this.ten*10+this.five*5+this.quarter*0.25+this.dime*0.1+this.nickel*0.05+this.penny*0.01;	
	}

	public void setOne(int one) {
		this.one = one;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public int getDime() {
		return dime;
	}

	public void setDime(int dime) {
		this.dime = dime;
	}

	public int getNickel() {
		return nickel;
	}

	public void setNickel(int nickel) {
		this.nickel = nickel;
	}

	public int getPenny() {
		return penny;
	}

	public void setPenny(int penny) {
		this.penny = penny;
	}

	@Override
	public String toString() {
		return "Datos [onehundred=" + onehundred + ", twenty=" + twenty + ", ten=" + ten + ", five=" + five + ", one="
				+ one + ", quarter=" + quarter + ", dime=" + dime + ", nickel=" + nickel + ", penny=" + penny
				+ ", totalmoney=" + totalmoney + "]";
	}
	
}