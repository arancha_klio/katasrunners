export function romannumeralconverter(num) {
    const romanos = {
        M: 1000,
        CM: 900,
        D: 500,
        CD: 400,
        C: 100,
        XC: 90,
        L: 50,
        XL: 40,
        X: 10,
        IX: 9,
        V: 5,
        IV: 4,
        I: 1,
      }
    
      let resultado = ""
      for (let r in romanos) {
        //repeat es parte del iterador y lo que hace es que
        // repite r las veces que sean necesarias. ( dependiendo la division)
        resultado += r.repeat(Math.floor( num / romanos[r]))
        num = num % romanos[r]
      }
      return resultado
    }