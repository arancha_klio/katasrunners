import { gof } from '../src/gameoflife'

// 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
// 2. Any live cell with more than three live neighbours dies, as if by overcrowding.
// 3. Any live cell with two or three live neighbours lives on to the next generation.
// 4. Any dead cell with exactly three live neighbours becomes a live cell.



describe('liveAnd1Neighbour', () => {
    //1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 1
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)

        gameoflife.joc(8)
    })

})

describe('liveAnd0Neighbour', () => {
    //1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 0
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd4Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 4
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd5Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 5
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd6Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 6
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd7Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 7
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd8Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 8
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.mort
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd2Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 2
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.viu
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('liveAnd3Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 3
        const valorinicial = gameoflife.viu
        const expected_value = gameoflife.viu
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('deadAnd3Neighbour', () => {
    //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
    it('should return die', () => {
        // Arrange
        const gameoflife = new gof()
        const nvecinos = 8
        const valorinicial = gameoflife.mort
        const expected_value = gameoflife.viu
        
        // Act
        const result = gameoflife.resuelveVida(valorinicial, nvecinos)
        
        // Assert
        expect(result).toBe(expected_value)
    })
})
