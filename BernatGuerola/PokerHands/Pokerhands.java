import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Pokerhands {

	public static String[] baraja = { "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A" };

	public static HashMap<String, String> mano(String[] cartas) {
		HashMap<String, String> mano = new HashMap<String, String>();
		String ks = "", valor = "";

		for (int i = 0; i < baraja.length; i++) {
			int k = 0;
			for (int c = 0; c < cartas.length; c++) {
				if (baraja[i] == cartas[c]) {
					valor = cartas[c];
					k++;
				}
			}
			if (k == 2) {
				ks = "Par";
			} else if (k == 3) {
				ks = "Trío";
			} else if (k == 4) {
				ks = "Poker";
			} else if (k == 5) {
				ks = "Repoker";
			}

			if (k > 1) {
				mano.put(ks, valor);
			}
		}

		return mano;
	}

	public int posicionenBaraja(String carta) {
		int posicion = 0;
		for (int i = 0; i < baraja.length; i++) {
			if (baraja[i] == carta) {
				posicion = i;
			}
		}
		return posicion;
	}

	public String devuelveMasAlta(String cartauno, String cartados) {
		int posicioncartauno = posicionenBaraja(cartauno);
		int posicioncartados = posicionenBaraja(cartados);
		int posicioncartamasalta = Math.max(posicioncartauno, posicioncartados);
		return baraja[posicioncartamasalta];
	}

	public String[] devuelveManoMasAlta(String[] manouno, String[] manodos) {
		String mayorvalormanouno = manouno[0];
		String mayorvalormanodos = manodos[0];

		for (int i = 1; i < manouno.length; i++) {
			mayorvalormanouno = devuelveMasAlta(manouno[i], mayorvalormanouno);
		}

		for (int i = 1; i < manouno.length; i++) {
			mayorvalormanodos = devuelveMasAlta(manodos[i], mayorvalormanodos);
		}

		if (devuelveMasAlta(mayorvalormanouno, mayorvalormanodos) == mayorvalormanouno) {
			return manouno;
		} else {
			return manodos;
		}
	}

	public String obtenerPar(String[] mano) {
		String aux = null;
		for (int i = 0; i < mano.length; i++) {
			int x = 0;
			for (int j = 0; j < baraja.length; j++) {
				if (mano[i] == baraja[j]) {
					x++;
					if (x == 2) {
						aux = mano[i];
					}
				}
				;
			}
			;
		}
		;
		return aux;
	}

	public String[] devuelveParMasAlto(String[] manouno, String[] manodos) {
		HashMap<String, String> resultmanouno = mano(manouno);
		HashMap<String, String> resultmanodos = mano(manodos);
		String auxmano1 = null;
		String auxmano2 = null;
		String[] result;

		for (Map.Entry<String, String> entry : resultmanouno.entrySet()) {
			if (entry.getKey() == "Par") {
				auxmano1 = entry.getValue();
			}
		}

		for (Map.Entry<String, String> entry : resultmanodos.entrySet()) {
			if (entry.getKey() == "Par") {
				auxmano2 = entry.getValue();
			}
		}

		int posiciocaramanouno = posicionenBaraja(auxmano1);
		int posiciocaramanodos = posicionenBaraja(auxmano2);

		if (posiciocaramanouno > posiciocaramanodos) {
			result = manouno;
		} else if (posiciocaramanouno < posiciocaramanodos) {
			result = manodos;
		} else {
			result = devuelveManoMasAlta(manouno, manodos);
		}

		return result;

	}

	public String[] mayorDeTres(String[] manouno, String[] manodos) {
		HashMap<String, String> resultmanouno = mano(manouno);
		HashMap<String, String> resultmanodos = mano(manodos);
		String auxmano1 = null;
		String auxmano2 = null;
		String[] result;

		for (Map.Entry<String, String> entry : resultmanouno.entrySet()) {
			if (entry.getKey() == "Trío") {
				auxmano1 = entry.getValue();
			}
		}

		for (Map.Entry<String, String> entry : resultmanodos.entrySet()) {
			if (entry.getKey() == "Trío") {
				auxmano2 = entry.getValue();
			}
		}

		int posiciocaramanouno = posicionenBaraja(auxmano1);
		int posiciocaramanodos = posicionenBaraja(auxmano2);

		if (posiciocaramanouno > posiciocaramanodos) {
			result = manouno;
		} else
			result = manodos;

		return result;
	}

	public String[] escaleraMasAlta(String[] manouno, String[] manodos) {
		return devuelveManoMasAlta(manouno, manodos);
	}

	public String[] trioMasPar(String[] manouno, String[] manodos) {
		HashMap<String, String> resultmanouno = mano(manouno);
		HashMap<String, String> resultmanodos = mano(manodos);
		String auxtrmano1 = null;
		String auxtrmano2 = null;
		String auxprmano1 = null;
		String auxprmano2 = null;
		String[] result;

		for (Map.Entry<String, String> entry : resultmanouno.entrySet()) {
			if (entry.getKey() == "Trío") {
				auxtrmano1 = entry.getValue();
			}
			if (entry.getKey() == "Par") {
				auxprmano1 = entry.getValue();
			}
		}

		for (Map.Entry<String, String> entry : resultmanodos.entrySet()) {
			if (entry.getKey() == "Trío") {
				auxtrmano2 = entry.getValue();
			}
			if (entry.getKey() == "Trio") {
				auxprmano2 = entry.getValue();
			}
		}

		int posiciocaramanouno = posicionenBaraja(auxtrmano1);
		int posiciocaramanodos = posicionenBaraja(auxtrmano2);

		if ((posiciocaramanouno > posiciocaramanodos) && (auxprmano1 != null && auxprmano2 != null)) {
			result = manouno;
		} else
			result = manodos;

		return result;
	}

	public String[] PokerMasAlto(String[] manouno, String[] manodos) {
		{
			HashMap<String, String> resultmanouno = mano(manouno);
			HashMap<String, String> resultmanodos = mano(manodos);
			String auxmano1 = null;
			String auxmano2 = null;
			String[] result;

			for (Map.Entry<String, String> entry : resultmanouno.entrySet()) {
				if (entry.getKey() == "Poker") {
					auxmano1 = entry.getValue();
				}
			}

			for (Map.Entry<String, String> entry : resultmanodos.entrySet()) {
				if (entry.getKey() == "Poker") {
					auxmano2 = entry.getValue();
				}
			}

			int posiciocaramanouno = posicionenBaraja(auxmano1);
			int posiciocaramanodos = posicionenBaraja(auxmano2);

			if (posiciocaramanouno > posiciocaramanodos) {
				result = manouno;
			} else if (posiciocaramanouno < posiciocaramanodos) {
				result = manodos;
			} else {
				result = devuelveManoMasAlta(manouno, manodos);
			}

			return result;

		}
	}

	public static void main(String[] args) {
		Pokerhands prova = new Pokerhands();
		// System.out.println(prova.devuelveMasAlta("8", "J"));

		String[] manouno = { "2", "3", "4", "3", "5" };
		String[] manodos = { "2", "5", "4", "5", "3" };

		String[] result = prova.devuelveParMasAlto(manouno, manodos);

		HashMap<String, String> res1 = mano(manouno);
		HashMap<String, String> res2 = mano(manodos);

		for (Map.Entry<String, String> entry : res1.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
		}
		for (Map.Entry<String, String> entry : res2.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
		}

		Stream.of(result).forEach(System.out::print);

	}

}
