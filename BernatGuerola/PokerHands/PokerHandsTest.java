import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class PokerHandsTest {
	Pokerhands tester = new Pokerhands();

	@Test
	void devuelveMayorDeDosCartas() {
		String obtenido = tester.devuelveMasAlta("4", "J");
		String esperado = "J"; 
		assertEquals(esperado,obtenido);
	}
	
	@Test
	void devuelveManoMasAlta() {
		String[] manouno = { "2", "3", "J", "8", "9" };
		String[] manodos = { "4", "5", "Q", "J", "A" };
		String[] obtenido = tester.devuelveManoMasAlta(manouno, manodos);
		String[] esperado = manodos;
		assertArrayEquals(esperado, obtenido);
	}
	
	@Test
	void ganaParMasAlto() {
		String[] manouno = {"2", "3", "4", "3", "5" };
		String[] manodos = {"2", "5", "4", "5", "3"};
		String[] obtenido =tester.devuelveParMasAlto(manouno, manodos);
		String[] esperado =manodos;
		assertArrayEquals(esperado, obtenido);
	}
	
	@Test
	void GanaCartaMasAltaConParIgual() {
		String[] manouno = {"2", "3", "4", "3", "A" };
		String[] manodos = {"2", "3", "4", "3", "J"};
		String[] obtenido =tester.devuelveParMasAlto(manouno, manodos);
		String[] esperado =manouno;
		assertArrayEquals(esperado, obtenido);	
	}
		
	@Test
	void GanaTrioMasAlto() {
		String[] manouno = {"2", "3", "3", "3", "A" };
		String[] manodos = {"2", "4", "4", "4", "J"};
		String[] obtenido =tester.mayorDeTres(manouno, manodos);
		String[] esperado =manodos;
		assertArrayEquals(esperado, obtenido);	
	}
	
	//	Straight: Hand contains 5 cards with consecutive values. 
	//Hands which both contain a straight are ranked by their highest card.
	@Test
	void GanaEscaleraMasAlta() {
		String[] manouno = {"2", "3", "4", "5", "6"};
		String[] manodos = {"3", "4", "5", "6", "7"};
		String[] obtenido =tester.escaleraMasAlta(manouno, manodos);
		String[] esperado =manodos;
		assertArrayEquals(esperado, obtenido);	
	}
	
	@Test
	void GanaTrioMasParMasAlto() {
		String[] manouno = {"3", "3", "3", "5", "5"};
		String[] manodos = {"4", "4", "4", "2", "2"};
		String[] obtenido =tester.trioMasPar(manouno, manodos);
		String[] esperado =manodos;
		assertArrayEquals(esperado, obtenido);		
	}
	
	@Test
	void GanaPoker() {
		String[] manouno = {"2", "2", "2", "2", "6"};
		String[] manodos = {"3", "3", "3", "3", "K"};
		String[] obtenido =tester.PokerMasAlto(manouno, manodos);
		String[] esperado =manodos;
		assertArrayEquals(esperado, obtenido);	
	}
	
	
}
