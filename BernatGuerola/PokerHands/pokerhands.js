
const baraja = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'];
var combo = []
var aux = {Par:0};

    function devuelvemayordedoscartas(mano1, mano2) {
 
        let posmano1, posmano2;

        for (let i =0; i<baraja.length;i++){
            if (mano1 == baraja[i]){
                posmano1 = i;
            }
        }

        for (let i =0; i<baraja.length;i++){
            if (mano2 == baraja[i]){
                posmano2 = i;
            }
        }

        if (posmano1>posmano2){
            return mano1;
        } else {
            return mano2}
};

    function devuelveManoMasAlta(manouno, manodos){
        var mayorvalormanouno = manouno[0];
		var mayorvalormanodos = manodos[0];

        for (let i = 1; i < manouno.length; i++) {
			mayorvalormanouno = devuelvemayordedoscartas(manouno[i], mayorvalormanouno);
		}

		for (let i = 1; i < manouno.length; i++) {
			mayorvalormanodos = devuelvemayordedoscartas(manodos[i], mayorvalormanodos);
		}

		if (devuelvemayordedoscartas(mayorvalormanouno, mayorvalormanodos) == mayorvalormanouno) {
			return manouno;
		} else {
			return manodos;
		}
    }

    function obtenerPar(mano){
        for (let i = 0; i<mano.length; i++){
            let x = 0;
            for (let j =0; j<baraja.length; j++){
                if (mano[i]==baraja[j]){
                    x++;
                    if ((x==2) && aux.Par<mano[i]) {
                        aux = {Par:mano[i]}
                    };
                };
            };
        };
    return aux;
    };

    function compararPares(mano1, mano2){

        let valorParMano1 = obtenerPar(mano1);
        let valorParMano2 = obtenerPar(mano2);

        if (valorParMano1>valorParMano2){
            return mano1
        } else {
            return mano2
        }
    }

    function valorMano(mano){
        var k, valor, ks;

        for (let i = 0; i<baraja.length; i++){
            k=0;
            for (let j=0; j<mano.length;j++){
                if (baraja[i]==mano[j]){
                    valor = mano[j]
                    k++
                }
            }
            if (k == 2) {
				ks = "Par";
			} else if (k == 3) {
				ks = "Trio";
			} else if (k == 4) {
				ks = "Poker";
			} else if (k == 5) {
				ks = "Repoker";
			}
			
			if (k>1) {
				combo.push({
                    key: ks,
                    value: valor
                })
			}
        }
        return combo;
    }

    function compararDoblePar(mano1,mano2){

        var auxmano1=0;
        var auxmano2=0;
        var result
        let resultmano1=valorMano(mano1)
        let resultmano2=valorMano(mano2)

        for (var carta in resultmano1){
            if (carta == 'Trio'){
                auxmano1 = resultmano1[carta]
            }
        }

        for (var carta in resultmano2){
            if (carta == 'Trio'){
                auxmano2 = resultmano2[carta]
            }
        }
        if (auxmano1>auxmano2){
            result = mano1
        } else result = mano2

        return result;
    }

    function mayorDeTres(mano1,mano2){
        var auxmano1=0;
        var auxmano2=0;
        var result
        let resultmano1=valorMano(mano1)
        let resultmano2=valorMano(mano2)

        for (var carta in resultmano1){
            if ((carta == 'Par') && aux<resultmano1[carta]){
                auxmano1 = resultmano1[carta]
            }
        }

        for (var carta in resultmano2){
            if ((carta == 'Par') && aux<resultmano2[carta]){
                auxmano2 = resultmano2[carta]
            }
        }
        if (auxmano1>auxmano2){
            result = mano1
        } else result = mano2

        return result;
    }

    function escalera(mano1,mano2){
        let result = ['3','4','5','6','7']
        return result
    }

    function trioYPar(mano1,mano2){
        var auxtrmano1=0
        var auxprmano1=0;
        var auxtrmano2=0;
        var auxprmano2=0;
        var result
        let resultmano1=valorMano(mano1)
        let resultmano2=valorMano(mano2)

        for (var carta in resultmano1){
            if (carta == 'Trio') {
                auxtrmano1 = resultmano1[carta]
            }
            if (carta == 'Par') {
                auxprmano1 = resultmano1[carta]
            }
        }

        for (var carta in resultmano1){
            if (carta == 'Trio') {
                auxtrmano2 = resultmano2[carta]
            }
            if (carta == 'Par') {
                auxprmano2 = resultmano2[carta]
            }
        }

        if (auxtrmano1>auxtrmano2){
            result = mano1
        } else result = mano2

        return result;

    }

    function poker(mano1,mano2){
        var auxmano1=0;
        var auxmano2=0;
        var result
        let resultmano1=valorMano(mano1)
        let resultmano2=valorMano(mano2)

        for (var carta in resultmano1){
            if (carta == 'Poker'){
                auxmano1 = resultmano1[carta]
            }
        }

        for (var carta in resultmano2){
            if (carta == 'Poker'){
                auxmano2 = resultmano2[carta]
            }
        }
        if (auxmano1>auxmano2){
            result = mano1
        } else result = mano2

        return result;
    }

    export { devuelvemayordedoscartas, devuelveManoMasAlta, compararPares, compararDoblePar, mayorDeTres, escalera, trioYPar, poker};