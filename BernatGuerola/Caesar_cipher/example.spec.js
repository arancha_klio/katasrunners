import { caesarcipher } from '../src/caesarcipher.js'

describe('Example', () => {
    it('should return FREE CODE CAMP', () => {
        // Arrange
        const expected_value = 'FREE CODE CAMP'

        // Act
        const result = caesarcipher('SERR PBQR PNZC')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return FREE PIZZA!', () => {
        // Arrange
        const expected_value = 'FREE PIZZA!'

        // Act
        const result = caesarcipher('SERR CVMMN!')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return FREE LOVE?', () => {
        // Arrange
        const expected_value = 'FREE LOVE?'

        // Act
        const result = caesarcipher('SERR YBIR?')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.', () => {
        // Arrange
        const expected_value = 'THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.'

        // Act
        const result = caesarcipher('GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT.')

        // Assert
        expect(result).toBe(expected_value)
    })
})
