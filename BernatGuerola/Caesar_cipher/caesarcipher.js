export function caesarcipher(str) {
    var abc='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    abc=abc.split('')
    str=str.split(' ')
    const exp = /[.*+\-¿?¡!^${}()|]/g
    var str3=''
    for (var x=0; x<str.length; x++){
      var str4=str[x].split('')
      for (var e=0; e<str4.length;e++){
        if (str4[e].match(exp)){
                str3+=str4[e]
            } else {
                for (var i=0; i<abc.length;i++){
                    if (str4[e]==abc[i]){
                      var c = i-13
                      if (c<0){
                          c=abc.length+c
                      }
                      str3 += abc[c]
                    }           
                }
              }
      }
      str3+=' '
    }
    str3=str3.slice(0,str3.length-1)
    return str3;
    
  }
