export class Marsrover {
    
    constructor(x,y,n){
        this.x = x;
		this.y = y;
		this.N = n;
    }

    toString() {
		return "MarsRover [" + this.x + ", "+ this.y + ", " + this.N +"]";
	}

    move(rover, s) {
		if (s!="") {
			let arrays=s.split("");
			let tam=arrays.length;
			
			for (let i=0;i<tam;i++) {
				let c=arrays[i];
				if (c=='M'){
                    this.y++
				} else if (c=='L') {
                    this.x--
                    this.N = 'W'
				} else if (c=='R') {
                    this.x++
                    this.N = 'E'
				}
			}
		}
		return rover.toString();
	}
}

