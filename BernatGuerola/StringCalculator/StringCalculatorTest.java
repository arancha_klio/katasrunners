package StringCalculator;

import static org.junit.jupiter.api.Assertions.*;

import java.security.Principal;

import org.junit.jupiter.api.Test;

class StringCalculatorTest {

	@Test
	void testreturnzero() {//'returns 0 for a empty string'
		String resultado=StringCalculator.calculator("");
		String esperado=String.valueOf("0");
		assertEquals(esperado,resultado);
	}

	@Test
	void testreturnsinglenumber() {//'returns the number for a single number string'
		String resultado=StringCalculator.calculator("6");
		String esperado=String.valueOf("6");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testreturnsum() {//'returns the sum of the numbers of the given string'
		String resultado=StringCalculator.calculator("3,6");
		String esperado=("9");
		assertEquals(esperado, resultado);
	}
	@Test
	void testreturnarray() {//'returns an array of numbers for a mixed number and symbols string'
		String resultado=StringCalculator.calculator("3\n2");
		String esperado=("5");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testnoreturmasmil() {//'returns only the sum of numbers smaller 1000'
		String resultado=StringCalculator.calculator("1,2,1005");
		String esperado=("3");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testnonegativenumbers() {//'should not allow negative numbers'
		String resultado=StringCalculator.calculator("1,2,-3");
		String esperado="negatives not allowed:"+"-3";
		assertEquals(esperado, resultado);
	}	
}
