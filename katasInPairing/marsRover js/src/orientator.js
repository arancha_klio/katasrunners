export class Orientator{
    constructor(orientation){
        this.orientation = orientation
    }

    getCurrentOrientation(){
        return this.orientation
    }

    turn(cardinalPoints){
        const indeCardinalPoint = cardinalPoints.indexOf(this.orientation)
        const next_orientation = cardinalPoints[indeCardinalPoint + 1]
        if (!next_orientation)
            return this.orientation = cardinalPoints[0]
        this.orientation = next_orientation
    }

    turnLeft(){
        const cardinalPoints = ['N', 'W', 'S', 'E']
        this.turn(cardinalPoints)
    }

    turnRight(){
        const cardinalPoints = ['N', 'E', 'S', 'W']
        this.turn(cardinalPoints)
    }
}