import {Orientator}  from './orientator.js'
import {Movements}  from './movements.js'

export class Rover {

    constructor(position){
        this.landing = position
        this.orientator = new Orientator(position[2])
        this.movements = new Movements(position[0], position[1])
    }
    
    landingPosition(){
        return this.landing
    }

    currentPosition(){
        return [...this.movements.currentPosition(), this.orientator.getCurrentOrientation()]  
    }

    command(commandList){
        commandList.forEach(command => {
            if(command == 'M'){this.movements.move(this.orientator.getCurrentOrientation())}
            if(command == 'L'){this.orientator.turnLeft()}
            if(command == 'R'){this.orientator.turnRight()}
        })
    }
}