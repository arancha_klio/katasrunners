import { fizzBuzz } from '../src/fizzBuzz.js'

describe('fizzBuzz', () => {
    it('should return a list', () => {
        // Arrange
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result instanceof Array).toBe(true)
    })

    it('should return a list of 1 to 100', () => {
        // Arrange
        const firstPosition = '1'
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[0]).toBe(firstPosition)

    })

    it('return "Fizz" if is divisible by 3', () => {
        // Arrange
        const expectValue = "Fizz"
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[2]).toBe(expectValue)
        expect(result[5]).toBe(expectValue)
    })

    it('return "Buzz" if is divisible by 5', () => {
        // Arrange
        const expectValue= "Buzz"
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[4]).toBe(expectValue)
        expect(result[9]).toBe(expectValue)
    })

    it('return "FizzBuzz" if is divisible by 3 and 5', () => {
        // Arrange
        const expectValue = "FizzBuzz"
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[14]).toBe(expectValue)
        expect(result[29]).toBe(expectValue)
    })

    it('return number as a string', () => {
        // Arrange
        // Act
        const result = fizzBuzz()
        // Assert
        expect(typeof result[0]).toBe('string')
    })

    it('returns "Fizz" if the number has a 3 in it', () => {
        //Arrange
        const expectValue = "Fizz"
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[36]).toBe(expectValue)
    })

    it('returns "Buzz" if the number has a 5 in it', () => {
        //Arrange
        const expectValue = "Buzz"
        // Act
        const result = fizzBuzz()
        // Assert
        expect(result[51]).toBe(expectValue)
    })

    it('returns dinamically a range of numbers in the list', () => {
        // Arrange
        const range = 1000
        // Act
        const result = fizzBuzz(range)
        // Assert
        expect(result.length).toBe(1000)
    })


})

