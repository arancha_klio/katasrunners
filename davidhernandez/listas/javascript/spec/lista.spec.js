import Lista from '../src/lista.js'

describe('A list', () => {
    it('should know the initial position', () => {
        // Arrange
        const expected_value = 0

        // Act
        const list = new Lista()

        // Assert
        expect(list.getCurrentPosition()).toBe(expected_value)
    })

    it('Should know it`s length', () => {
        // Arrange
        const expected_value = 0

        // Act
        const list = new Lista()

        // Assert
        expect(list.getLength()).toBe(expected_value)
    })

    it('Should add new values', () => {
        // Arrange
        const expected_value = 1
        const list = new Lista()
        list.add('New Value')

        // Act
        const length = list.getLength()

        // Assert
        expect(length).toBe(expected_value)
    })

    it('Should retrieve items from the given position', () => {
        const expected_value = 'Value'
        const list = new Lista()
        list.add('Value')

        const item = list.get(0)

        expect(item).toBe(expected_value)
    })

    it('Should iterate it`s content returning the next value', () => {
        const expected_value = 'Second Value'
        const list = new Lista()
        list.add('Value')
        list.add('Second Value')

        list.next() // Returns 'Value'
        const second_value = list.next()

        expect(second_value).toBe(expected_value)
    })

    it('Should know if contains an item', () => {
        const expected_value = 1
        const list = new Lista()
        list.add('first Value')
        list.add('Value')

        const result = list.contains('Value')

        expect(result).toBe(expected_value)
    })

    it('Should know if it does not contain an item', () => {
        const expected_value = false
        const list = new Lista()
        list.add('Value')

        const result = list.contains('Non existing value')

        expect(result).toBe(expected_value)
    })

    it('Should delete items from a given position', () => {
        const expected_value = 1
        const list = new Lista()
        list.add('Value')
        list.add('New value')
        list.add('Other value')

        list.delete(0)

        expect(list.getLength()).toBe(expected_value)
    })

    it('Should export to list', () => {
        const expected_value = ['a', 'b', 'c']
        const list = new Lista
        list.add('a')
        list.add('b')
        list.add('c')

        const exportedList = list.export()

        expect(exportedList).toEqual(expected_value)
    })
})
