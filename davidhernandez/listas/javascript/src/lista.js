export default class Lista {
    constructor() {
        this.current_position = 0
        this.length = 0
    }

    get(position) {
        return this[this.getItemName(position)]
    }

    getCurrentPosition() {
        return this.current_position
    }

    getLength() {
        return this.length
    }

    add(item) {
        this[this.getItemName(this.length)] = item
        this.length++
    }

    next() {
        const item = this.get(this.getCurrentPosition())
        this.current_position++
        return item
    }

    getItemName(position) {
        return 'item_' + position
    }

    contains(needle) {
        let position = 0

        while(position < this.length) {
            const item = this.get(position)
            if (item == needle) {
                return position
            }
            position++
        }
        return false
    }

    delete(position) {
        if (position == this.length) {
            delete this[this.getItemName(position)]
            this.length--
        }
        else {
            const currentItemName = this.getItemName(position)
            const nextItemName = this.getItemName(position + 1)

            this[currentItemName] = this[nextItemName]
            this.delete(position + 1)
        }
    }

    export() {
        const output = []
        let position = 0
        while(position < this.length) {
            const item = this.get(position)
            output.push(item)
            position++
        }

        return output
    }
}