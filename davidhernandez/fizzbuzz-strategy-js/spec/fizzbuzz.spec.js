import { FizzBuzz, Rule, FizzRule, FizzBuzzRule, BuzzRule } from '../src/FizzBuzz'

const rules = [
    new FizzBuzzRule(),
    new FizzRule(),
    new BuzzRule(),
    new Rule(),
]

describe('FizzBuzz', () => {
    it('should return Fizz if it is multiple of three', () => {
        // Arrange
        const expected_value = 'Fizz'
        const fizzBuzz = new FizzBuzz(3)

        // Act
        const result = fizzBuzz.convert(rules)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return Buzz if it is multiple of five', () => {
        // Arrange
        const expected_value = 'Buzz'
        const fizzBuzz = new FizzBuzz(5)

        // Act
        const result = fizzBuzz.convert(rules)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return FizzBuzz if it is multiple of three and five', () => {
        // Arrange
        const expected_value = 'FizzBuzz'
        const fizzBuzz = new FizzBuzz(15)

        // Act
        const result = fizzBuzz.convert(rules)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return the number as string if it is not multiple of three or five', () => {
        // Arrange
        const expected_value = '4'
        const fizzBuzz = new FizzBuzz(4)

        // Act
        const result = fizzBuzz.convert(rules)

        // Assert
        expect(result).toBe(expected_value)
    })
})
