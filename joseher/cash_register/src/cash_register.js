const moneyEquivalence = {
    PENNY: 0.01,
    NICKEL: 0.05,
    DIME: 0.1,
    QUARTER: 0.25,
    ONE: 1,
    FIVE: 5,
    TEN: 10,
    TWENTY: 20,
    HUNDRED: 100
}

export function checkCashRegister(price, cash, cid) {
    let change = []
    let status = "OPEN"
   
    const TOTAL_AMOUNT_TO_RETURN = round(cash - price)
    const TOTAL_IN_DRAWER = round(calculateTotalCashInDrawer(cid))

    if (TOTAL_AMOUNT_TO_RETURN == 0) {
        return { status, change }
    }

    if (TOTAL_AMOUNT_TO_RETURN > TOTAL_IN_DRAWER) {
        status = "INSUFFICIENT_FUNDS"
    }

    if (TOTAL_AMOUNT_TO_RETURN == TOTAL_IN_DRAWER) {
        change = cid
        status = "CLOSED"
    }

    if (TOTAL_AMOUNT_TO_RETURN < TOTAL_IN_DRAWER) {
        change = calculateChange(TOTAL_AMOUNT_TO_RETURN, cid)
    }

    return { status, change }
}

function round(number) {
    return Math.round(number * 100) / 100
}

function calculateTotalCashInDrawer(cid) {
    const callback = (accumulator, coinAmount) => {
        return accumulator + coinAmount[1]
    }
    return cid.reduce(callback, 0)
}

function calculateChange(changeTotalAmount, cid) {
    let changeToReturn = []
    

    cid.slice().reverse().forEach(coinAmount => {
        const currentCoinName = coinAmount[0]
        let currentCoinCiD = coinAmount[1]
        const currentCoinValue = moneyEquivalence[currentCoinName]
        let currentCoinGivenChange = 0

        while(currentCoinCiD > 0 && changeTotalAmount >= currentCoinValue) {
            currentCoinGivenChange = round(currentCoinGivenChange + currentCoinValue)
            currentCoinCiD = round(currentCoinCiD - currentCoinValue)
            changeTotalAmount = round(changeTotalAmount - currentCoinValue)
        }

        if (currentCoinGivenChange > 0) {
            changeToReturn.push([currentCoinName, currentCoinGivenChange])
        }
    })
    return changeToReturn
} 