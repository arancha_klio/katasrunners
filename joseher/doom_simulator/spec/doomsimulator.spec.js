import { DoomSimulator } from '../src/doomsimulator'

describe('Doom Simulator', () => {
    it('the player has 100 of max HP and 50 of max Armor', () => {
        // Arrange
        const doom_game = new DoomSimulator()
        const expected_value =  { health:100, armor:50 }

        // Act
        const result = doom_game.player

        // Assert
        expect(result.health).toEqual(expected_value.health)
        expect(result.armor).toEqual(expected_value.armor)
    })

    it('at the start the player gets 3 random weapons', () => {
        // Arrange
        const doom_game = new DoomSimulator()
        const expected_value = 4

        // Act 
        doom_game.obtainWeapons()
        const result = Object.keys(doom_game.player_weapons).length

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('the level starts with 6 random demons', () => {
        // Arrange
        const doom_game = new DoomSimulator()
        const expected_value = 6

        // Act 
        doom_game.obtainDemons()
        const result = doom_game.level_demons.length

        // Assert
        expect(result).toEqual(expected_value)
    })
    it('the level starts with 6 random demons', () => {
        // Arrange
        const doom_game = new DoomSimulator()
        const expected_value = 6

        // Act 
        const result = doom_game.game()

        // Assert
        expect(result).toEqual(expected_value)
    })
})
